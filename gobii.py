'''
Created on Jul 31, 2019

@author:     John Carlos Ignacio,

@copyright:  2019 International Rice Research Institute. All rights reserved.

@license:    MIT License

@contact:    jii5@cornell.edu
'''
import os

import math
import pandas as pd
import copy
from sys import stderr
from subprocess import call
from subprocess import Popen
from itertools import izip_longest
from collections import OrderedDict
import shutil

class consensus:
    '''
    Class for consensus genotype calling
    '''

    def __init__(self, sample_data, genotype_data, group_column='germplasm_name', sep='/', mis='N', threshold=0.5):
        '''
        Constructor
        '''
        if not isinstance(sample_data, pd.DataFrame):
            raise TypeError("sample_data must be a pandas dataframe")
        if not isinstance(genotype_data, pd.DataFrame):
            raise TypeError("genotype_data must be a pandas dataframe")
        self.sample_data = sample_data
        self.genotype_data = genotype_data
        self.group_column = group_column
        self.sep = sep
        self.mis = mis
        self.threshold = threshold
        self.encoded_genotype = None
        self.cgenotype_data = None
        self.csample_data = None
        
    def get_consensus_data(self):
        return self.genotype_data, self.sample_data
    
    def create_encoding_dict(self, allele1, allele2, sep=None, mis=None):
        sep = self.sep
        mis = self.mis
        enc_dict = {
                allele1 + sep + allele1 : 0,
                allele1 + sep + allele2 : 1,
                allele2 + sep + allele1 : 1,
                allele2 + sep + allele2 : 3,
                mis + sep + mis : -1,
            }
        return enc_dict

    def create_recoding_dict(self, allele1, allele2, sep, mis):
        enc_dict = {
                0 : allele1 + sep + allele1,
                1 : allele1 + sep + allele2,
                2 : allele2 + sep + allele1,
                3 : allele2 + sep + allele2,
                -1 : mis + sep + mis,
            }
        return enc_dict
    
    def get_alleles(self, genotype_data, sep, mis):
        allele_matrix = {}
        for column in genotype_data:
            #get alleles
            alleles = list(set(sep.join(genotype_data[column].drop_duplicates()).split(sep)))
            if mis in alleles:
                alleles.remove(mis)
            while len(alleles) < 2:
                alleles.append('X')
            alleles.sort()
            allele_matrix[column] = alleles
        return allele_matrix
    
    def count_genotype(self, variant):
        genotypes = [0,1,2,3]
        genotype_counts = []
        x = list(variant)
        for geno in genotypes:
            genotype_counts.append(x.count(geno))
        return genotype_counts
    
    def encode_genotypes(self, genotype_data, allele_matrix, sep, mis):
        genotype_data = copy.deepcopy(genotype_data)
        for column in genotype_data:
            #get alleles
            encoding_dict = self.create_encoding_dict(allele_matrix[column][0], allele_matrix[column][1], sep, mis)
            genotype_data[column] = genotype_data[column].map(encoding_dict)
        return genotype_data
    
    def recode_genotypes(self, genotype_data, allele_matrix, sep, mis):
        for column in genotype_data:
            #get alleles
            recoding_dict = self.create_recoding_dict(allele_matrix[column][0], allele_matrix[column][1], sep, mis)
            genotype_data[column] = genotype_data[column].map(recoding_dict)
        return genotype_data

    def make_consensus(self):
        sample_data = self.sample_data
        genotype_data = self.genotype_data
        group_column = self.group_column
        sep = self.sep
        mis = self.mis
        threshold = self.threshold
        
        group_list = sample_data[group_column].drop_duplicates()
        allele_matrix = self.get_alleles(genotype_data, sep, mis)
        self.encoded_genotype = self.encode_genotypes(genotype_data, allele_matrix, sep, mis)
        genotype_data = self.encoded_genotype.astype('int8')
        
        genotypes = [0,1,2,3,-1]
        
        tied_genotypes_dict = {
            '01' : 0,
            '02' : 0,
            '13' : 3,
            '23' : 3,
            '03' : -1,
            }
        
        column_names = [col for col in genotype_data]
        new_geno_data = pd.DataFrame(columns=column_names)
        new_sample_data = pd.DataFrame(columns=[col for col in sample_data]) 
        
        for index,item in group_list.iteritems():
            if isinstance(item, float):
                if math.isnan(item):
                    continue
            elif isinstance(item, str):
                if not item:
                    continue
            df_sample = sample_data[sample_data[group_column] == item]
            df_genotype = genotype_data.loc[df_sample.index.tolist(),:]
            
            count_matrix = {}
            freq_matrix = {}
            max_matrix = {}
            
            consensus_genotype = []
            
            for variant in df_genotype:
                count = self.count_genotype(df_genotype[variant])
                if sum(count) == 0:
                    consensus_genotype.append(-1)
                    continue
                freq = [x/float(sum(count)) for x in count]
                max_count = max(count)
                max_count_index = count.index(max_count)
                max_freq = freq[max_count_index]
                
                if max_freq > threshold: # set majority rule
                    consensus_genotype.append(genotypes[max_count_index])
                elif count.count(max_count) == 2: # look for 2-way tie and set according to dict
                    # get tied genotypes
                    tied_indices = [i for i, x in enumerate(count) if x == count[max_count_index]]
                    tied_genotypes = [str(genotypes[x]) for x in tied_indices]
                    consensus_genotype.append(tied_genotypes_dict["".join(tied_genotypes)])
                else: # set to missing
                    consensus_genotype.append(-1)
                        
                count_matrix[variant] = count
                freq_matrix[variant] = freq
                max_matrix[variant] = [max_count, max_count_index]

            
            new_geno_data.loc[item+'*'] = consensus_genotype
            
            tmp_df_sample = pd.DataFrame({group_column:item},index=[item+'*'])
            new_sample_data = pd.concat([new_sample_data,tmp_df_sample],axis=0, sort=False)
                                
        new_geno_data = self.recode_genotypes(new_geno_data, allele_matrix, sep, mis)
        
        self.cgenotype_data = new_geno_data
        self.csample_data = new_sample_data

class dataset:
    '''
    class of a dataset composed of genotype and sample information
    '''

    def __init__(self, genotype_data, sample_data, name=None, parents=None,
                 samples=None, parent_columns=None, parent_search_column=None, sep='/', mis='N'):
        self.genotype_data = genotype_data
        self.sample_data = sample_data
        self.nsamples = self.genotype_data.shape[0]
        self.nmarkers = self.genotype_data.shape[1]
        self.name = name
        self.parents = parents
        self.samples = samples
        self.parent_columns = parent_columns
        self.parent_search_column = parent_search_column
        self.index_of_parents = None
        self.sep = sep
        self.mis = mis
        self.sample_call_rates = []
        self.marker_call_rates = []

    def export_genotype_data_to_file(self, filename):
        self.genotype_data.to_csv(filename, index=True, na_rep='', sep="\t", mode="w", line_terminator="\n")

    def export_sample_data_to_file(self, filename):
        self.sample_data.to_csv(filename, index=True, na_rep='', sep="\t", mode="w", line_terminator="\n")

    def export_to_file(self, filename):
        self.export_genotype_data_to_file(filename+'.gt.txt')
        self.export_sample_data_to_file(filename+'.sm.txt')

    def set_name(self, name):
        self.name = name

    def set_parents(self, parent_columns=None):
        if parent_columns is not None:
            self.parent_columns = parent_columns
        elif self.parent_columns is None:
            raise TypeError("Parent column has not been set,"
                            "please specify parent_columns e.g. ['germplasm_par1','germplasm_par2']")
        parents = []
        for column in self.parent_columns:
            parents = parents + self.sample_data[column].tolist()
        #Drop duplicates and nan
        parents = list(dict.fromkeys(parents))
        self.parents = set([x for x in parents if str(x) != 'nan'])

    def set_index_of_parents(self, parent_search_column=None):
        if parent_search_column is None:
            if self.parent_search_column is not None:
                parent_search_column = self.parent_search_column
        self.parent_search_column = parent_search_column
        self.index_of_parents = [idx for idx, x in self.sample_data.iterrows() if x[parent_search_column] in self.parents]

    def get_parent_genotype_data(self):
        if self.parents is None:
            self.set_parents(self.parent_columns)
        if self.index_of_parents is None:
            self.set_index_of_parents()
        gd = self.genotype_data.loc[self.index_of_parents]
        return gd

    def get_parent_sample_data(self):
        if self.parents is None:
            self.set_parents(self.parent_columns)
        if self.index_of_parents is None:
            self.set_index_of_parents()
        sd = self.sample_data.loc[self.index_of_parents]
        return sd

    def concat(self, ds):
        self.genotype_data = self.genotype_data.append(ds.genotype_data)
        self.sample_data = self.sample_data.append(ds.sample_data)

    def get_parent_data(self):
        if self.parents is None:
            self.set_parents(self.parent_columns)
        if self.index_of_parents is None:
            self.set_index_of_parents()
        gd = self.genotype_data.loc[self.index_of_parents]
        sd = self.sample_data.loc[self.index_of_parents]
        new_ds = dataset(genotype_data=gd, sample_data=sd)
        new_ds.parent_columns = self.parent_columns
        new_ds.parent_search_column = self.parent_search_column
        new_ds.sep = self.sep
        new_ds.mis = self.mis
        return new_ds

    def filter_genotype_data(self, column, value):
        gd = self.genotype_data
        sd = self.sample_data
        idx = [x == value for x in sd[column]]
        return gd.loc[idx]

    def filter_sample_data(self, column, value):
        sd = self.sample_data
        idx = [x == value for x in sd[column]]
        return sd.loc[idx]

    def filter(self, idx=None, column=None, value=None):
        gd = self.genotype_data
        sd = self.sample_data
        if idx is None:
            if column is None or value is None:
                raise TypeError("Please specify indices, or column and value, to filter data with.")
            idx = [x == value for x in sd[column]]
        return dataset(gd.loc[idx], sd.loc[idx])

    def filter_for_parents_of_y(self, y, parent_search_column=None):
        x = dataset(self.genotype_data, self.sample_data)
        x.parents = y.parents
        x.parent_search_column = parent_search_column
        return x.get_parent_data()

    def filter_for_parents_of_y(self, y, parent_search_column=None):
        parents = set(y.parents)
        index_of_parents = [idx for idx, x in self.sample_data.iterrows() if x[parent_search_column] in parents]
        gd = self.genotype_data.loc[index_of_parents]
        sd = self.sample_data.loc[index_of_parents]
        return dataset(genotype_data=gd, sample_data=sd)

    def createFJProjectFile(self, file, name, separator='/', missing='N/N', qtlfile=None, mapfile=None, jarfile=None):
        sfile = file + '.sm.txt'
        gfile = file + '.gt.tmp'
        cmd = ['java', '-cp', jarfile, 'jhi.flapjack.io.cmd.CreateProject', '-A', '-g', gfile, '-t', sfile,
               '-p', 'output.flapjack', '-n', name, '-S', separator, '-M', missing, '-C']
        if qtlfile:
            cmd += ['-q', qtlfile]
        if mapfile:
            cmd += ['-m', mapfile]
        call(cmd)

    # def cleanUpFiles(self, file, name):

    def createFJHeader(self, name, headerjar):
        sfile = name + '.sm.txt'
        gfile = name + '.gt.txt'
        cmd = ['java', '-jar', headerjar, sfile, gfile, name + '.gt.tmp']
        p = Popen(cmd)
        p.wait()
        #call(cmd)

    def set_call_rates(self):
        self.sample_call_rates = []
        self.marker_call_rates = []

        shape = self.genotype_data.shape
        n_samples = float(shape[0])
        n_markers = float(shape[1])

        mis = self.mis + self.sep + self.mis

        for x, y in self.genotype_data.iteritems():
            if mis in set(y):
                self.marker_call_rates.append(round(1.0 - (float(list(y).count(mis)) / n_samples), 3))
            else:
                self.marker_call_rates.append(1.0)

        for x, y in self.genotype_data.iterrows():
            if mis in set(y):
                self.sample_call_rates.append(round(1.0 - (float(list(y).count(mis)) / n_markers), 3))
            else:
                self.sample_call_rates.append(1.0)

    def get_filter_index(self, sample_call_rate=None, marker_call_rate=None):
        if len(self.marker_call_rates) == 0 | len(self.sample_call_rates) == 0:
            self.set_call_rates()
        if marker_call_rate is not None:
            markers_to_keep = map(lambda x: x > marker_call_rate, self.marker_call_rates)
        else:
            markers_to_keep = self.genotype_data.columns
        if sample_call_rate is not None:
            samples_to_keep = map(lambda x: x > sample_call_rate, self.sample_call_rates)
        else:
            samples_to_keep = self.genotype_data.index
        return markers_to_keep, samples_to_keep

    # remove samples or markers below call rates
    def filter_genotype(self, sample_call_rate=None, marker_call_rate=None):
        markers_to_keep, samples_to_keep = self.get_filter_index(sample_call_rate, marker_call_rate)

        new_gd = self.genotype_data.loc[samples_to_keep, markers_to_keep]
        new_sd = self.sample_data.loc[self.genotype_data.index[samples_to_keep]]
        new_ds = dataset(new_gd, new_sd, parent_columns = self.parent_columns, parent_search_column = self.parent_search_column,
        sep = self.sep, mis = self.mis)
        return new_ds


class splitter:
    '''
    class for splitting datasets
    '''
    
    def __init__(self, dataset, germplasm_name_column='germplasm_name', par1='germplasm_par1', par2='germplasm_par2', sep='/', mis='N'):
        self.genotype_data = dataset.genotype_data
        self.sample_data = dataset.sample_data
        self.germplasm_name_column = germplasm_name_column
        self.par1 = par1
        self.par2 = par2
        self.sep = sep
        self.mis = mis
        self.group_column_names = None
        self.parents = None
        self.group_list = None
        self.group_idx = None

    def generate_split_dict(self, *cols):
        sample_data = self.sample_data
        queue = list(cols)
        idx = OrderedDict({'initdict':sample_data.index})
        while queue:
            col = queue.pop(0)
            tmp_idx = OrderedDict()
            out_idx = OrderedDict(map(lambda (x, y): (x, self.recursive_split(x, sample_data.loc[y], col)), idx.items()))
            map(lambda (x, y): tmp_idx.update(out_idx[x]), out_idx.items())
            idx = tmp_idx
        self.group_list = map(lambda (x, y): x, idx.items())
        self.group_idx = idx

    def split(self, *cols):
        if cols:
            self.generate_split_dict(*cols)
        split_dataset = OrderedDict(map(lambda (x, y): (x, dataset(self.genotype_data.loc[y],self.sample_data.loc[y])), self.group_idx.items()))
        return split_dataset

    def recursive_split(self, parent_name, sample_data, col):
        groups = sample_data[col].unique()
        valid_groups = [x for x in groups if pd.notna(x)]
        na_groups = [x for x in groups if pd.isna(x)]

        idx = OrderedDict(map(lambda x: (self.rename_group(parent_name, x), sample_data.loc[pd.isna(sample_data[col])].index), na_groups))

        idx.update(OrderedDict(map(lambda x: (self.rename_group(parent_name, x),
                                       sample_data.loc[sample_data[col] == x].index), valid_groups)))
        return idx

    def write_groups_to_file(self, dict, filename):
        f = open(filename, "w")
        for group, item in dict.items():
            f.write(group + "\n")
        f.close()

    def rename_group(self, parent_name, child_name):
        parent_name = str(parent_name)
        child_name = str(child_name)
        if parent_name == 'initdict':
            new_name = child_name
        else:
            if parent_name == 'nan':
                if child_name == 'nan':
                    new_name = 'nan'
                else:
                    new_name = 'nan_' + child_name
            elif child_name == 'nan':
                new_name = parent_name
            else:
                new_name = parent_name + '_' + child_name
        return new_name

    def get_parents(self):
        # Split sample file #
        group_list = self.sample_data[self.column_name1].drop_duplicates()
        mergedlist = []
        for index, item in group_list.iteritems():
            if isinstance(item, float):
                if math.isnan(item):
                    continue
            elif isinstance(item, str):
                if not item:
                    continue
            df = self.sample_data[self.sample_data[self.column_name1] == item]
            
            # store dnaruns of parents in a list
            par1 = list(set(filter(lambda x: str(x) != 'nan', df[self.par1])))
            par2 = list(set(filter(lambda x: str(x) != 'nan', df[self.par2])))
            lst1 = list(self.sample_data.loc[self.sample_data[self.germplasm_name_column].isin(par1),:].index)
            lst2 = list(self.sample_data.loc[self.sample_data[self.germplasm_name_column].isin(par2),:].index)
            mergedlist = mergedlist + lst1 + lst2
        self.parents = list(dict.fromkeys(mergedlist))
    
    def splitData(self):
        # Split sample file #
        group_list = self.sample_data[self.column_name1].drop_duplicates()
        for index, item in group_list.iteritems():
            if isinstance(item, float):
                if math.isnan(item):
                    continue
            elif isinstance(item, str):
                if not item:
                    continue
            df = self.sample_data[self.sample_data[self.column_name1] == item]
            
            # store dnaruns of parents in a list
            par1 = list(set(filter(lambda x: str(x) != 'nan', df['germplasm_par1'])))
            par2 = list(set(filter(lambda x: str(x) != 'nan', df['germplasm_par2'])))
            lst1 = list(self.sample_data.loc[self.sample_data['germplasm_name'].isin(par1),:].index)
            lst2 = list(self.sample_data.loc[self.sample_data['germplasm_name'].isin(par2),:].index)
            mergedlst = lst1 + lst2
                    
            subgroup_list = df[self.column_name2].drop_duplicates()
            for idx, sub in subgroup_list.iteritems():
                if isinstance(sub, float):
                    if math.isnan(sub):
                        if not item in self.parents and mergedlst:
                            self.parents.update({item : mergedlst})
                        continue
                elif isinstance(sub, str):
                    if not sub:
                        continue
    
                subkey = item+'_'+sub
                if not subkey in self.parents and mergedlst:
                    self.parents.update({subkey : lst1+lst2})
        
        # Split genotype file based on sample information #
        self.splitfile(self.genotype_data, self.sample_data, self.parents, group_list, subgroup_list)


    def createProjectFile(self, samplefile, genofile, jarfile, separator, missing, qtlfile, mapfile):
        sample_data = pd.read_table(samplefile, dtype='str')
        groups = sample_data.dnasample_sample_group.drop_duplicates()
        for index, key in groups.iteritems():
            if isinstance(key, float) and math.isnan(key):
                continue
            df = sample_data[sample_data.dnasample_sample_group == key]
            subgroup_list = df.dnasample_sample_group_cycle.drop_duplicates()
            for idx, sub in subgroup_list.iteritems():
                if isinstance(sub, float) and math.isnan(sub):
                    name = key
                elif isinstance(sub, str) and not sub:
                    name = key
                else:
                    name = key+'_'+sub
                name = str(name)
                sfile = samplefile+'_'+name+'.txt'
                gfile = genofile+'_'+name+'.txt.tmp'
                cmd = ['java', '-cp',jarfile,'jhi.flapjack.io.cmd.CreateProject','-A','-g',gfile,'-t',sfile,'-p',genofile+'.flapjack','-n',name,'-S',separator,'-M',missing,'-C']
                if qtlfile:
                    cmd += ['-q',qtlfile]
                if mapfile:
                    cmd += ['-m',mapfile]
                print(cmd)
                call(cmd)
        
    def createHeader(self, samplefile, genofile, headerjar):
        sample_data = pd.read_table(samplefile, dtype='str')
        groups = sample_data.dnasample_sample_group.drop_duplicates()
        for index, key in groups.iteritems():
            if isinstance(key, float) and math.isnan(key):
                continue
            df = sample_data[sample_data.dnasample_sample_group == key]
            subgroup_list = df.dnasample_sample_group_cycle.drop_duplicates()
            for idx, sub in subgroup_list.iteritems():
                if isinstance(sub, float) and math.isnan(sub):
                    name = key
                elif isinstance(sub, str) and not sub:
                    name = key
                else:
                    name = key+'_'+sub
                name = str(name)
                sfile = samplefile+'_'+name+'.txt'
                gfile = genofile+'_'+name+'.txt'
                cmd = ['java','-jar',headerjar,sfile,gfile,gfile+'.tmp']
                call(cmd)

    def batch_create_fj(self, path, names, separator='/', missing='N/N', qtlfile=None, mapfile=None, jarfile=None,
                        header=True, will_clean_up=True):
        bfile = path + 'batch.txt.tmp'
        qfile = 'qtl.txt.tmp'
        mfile = 'map.txt.tmp'
        fjp = path + 'output.flapjack'

        batch_file = open(bfile, "w")
        batch_file.write("map\tgeno\tsample\tqtl\tname\n")
        if qtlfile is None:
            qfile = ''
        else:
            shutil.copyfile(qtlfile, path + qfile)
        if mapfile is None:
            mfile = ''
        else:
            shutil.copyfile(mapfile, path + mfile)
        map(lambda (x, y): batch_file.write(self.get_ds_batch_line(x, qfile, mfile, header)), names.items())
        batch_file.close()
        if os.path.exists(fjp):
            os.remove(fjp)
        cmd = ['java', '-cp', jarfile, 'jhi.flapjack.io.cmd.CreateProjectBatch', '-b', bfile, '-A',
               '-p', fjp, '-S', separator, '-M', missing, '-C']
        call(cmd)
        if will_clean_up:
            self.clean_up(bfile)
            if qtlfile is not None:
                self.clean_up(path + qfile)
            if mapfile is not None:
                self.clean_up(path + mfile)
            map(lambda (x, y): self.clean_up(path + x + '.sm.txt'), names.items())
            map(lambda (x, y): self.clean_up(path + x + '.gt.txt'), names.items())
            if header:
                map(lambda (x, y): self.clean_up(path + x + '.gt.tmp'), names.items())


    def get_ds_batch_line(self, name, qtlfile, mapfile, header=True):
        sfile = name + '.sm.txt'
        if header is True:
            gfile = name + '.gt.tmp'
        else:
            gfile = name + '.gt.txt'
        line = mapfile + "\t" + gfile + "\t" + sfile + "\t" + qtlfile + "\t" + name + "\n"
        return line

    def create_fj_header(self, name, headerjar):
        sfile = name + '.sm.txt'
        gfile = name + '.gt.txt'
        cmd = ['java', '-jar', headerjar, sfile, gfile, name + '.gt.tmp']
        return cmd

    def parallel_fj_header_creation(self, path, names, headerjar, limit=4):
        commands  = map(lambda (x, y): self.create_fj_header(path + x, headerjar), names.items())

        groups = [(Popen(cmd) for cmd in commands)] * limit  # itertools' grouper recipe
        for processes in izip_longest(*groups):  # run len(processes) == limit at a time
            for p in filter(None, processes):
                p.wait()

    def clean_up(self, filename):
        if os.path.exists(filename):
            os.remove(filename)
            #print("Deleted %s" % filename)
        #else:
            #stderr.write("Failed to delete %s, file does not exist\n" % filename)

class qtl:
    '''
    class to store qtl data for qtl profiling and forward breeding analysis
    '''
    def __init__(self, qtl):
        self.qtl = qtl
        self.marker_groups = None
        self.haplo_dict = None
